import React, { useEffect } from 'react'

import { ToastContainer, toast } from 'react-toastify'
import ClipboardJS from 'clipboard'

import Header from './components/Header'
import EmojiButton from './components/EmojiButton'

import 'react-toastify/dist/ReactToastify.css'
import './App.css'

function App() {

    useEffect(() => {

        var clipboard = new ClipboardJS('.btn')
    
        clipboard.on('success', function(e) {
            console.info('Action:', e.action)
            console.info('Text:', e.text)
            console.info('Trigger:', e.trigger)

            toast.success(`${e.text} copy to clipboard!`, {
                position: "top-center",
                autoClose: 2000,
            })
        
            e.clearSelection()
        })
        
        clipboard.on('error', function(e) {
            console.error('Action:', e.action)
            console.error('Trigger:', e.trigger)
            toast.error('Error copying the emoji 😓', {
                position: "top-center",
            })
        })

    }, [])

    const emojiList = [
      {
        description: "Initial Commit",
        emoji: "🎉",
        text: ":tada:"
      },
      {
        description: "New Features",
        emoji: "✨",
        text: ":sparkles:"
      },
      {
        description: "Improve Performance",
        emoji: "⚡️",
        text: ":zap:"
      },
      {
        description: "Add/Update Development Scripts",
        emoji: "🔨",
        text: ":hammer:"
      },
      {
        description: "Change Values",
        emoji: "📏",
        text: ":straight_ruler:"
      },
      {
        description: "Releases",
        emoji: "🔖",
        text: ":bookmark:"
      },
      {
        description: "Bug Fixes",
        emoji: "🐛",
        text: ":bug:"
      },
      {
        description: "Remove Unused Stuff",
        emoji: "🔥",
        text: ":fire:"
      },
      {
        description: "Merge Branches",
        emoji: "🔀",
        text: ":twisted_rightwards_arrows:"
      },
      {
        description: "Revert Changes",
        emoji: "⏪",
        text: ":rewind:"
      },
      {
        description: "Work In Progress",
        emoji: "🚧",
        text: ":construction:"
      },
      {
        description: "Add/Change Easter Egg",
        emoji: "🥚",
        text: ":egg:"
      }
    ]
  return (
    <div>
      <Header />
      <p className='help-text'>Tap the emoji to copy it to clipboard</p>
      <div className="emoji-list">
        {emojiList.map(data => <EmojiButton key={data.text} data={data}/>)}
      </div>
      <ToastContainer />
    </div>
  );
}

export default App;
