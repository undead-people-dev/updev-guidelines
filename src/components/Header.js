import './Header.css'

function Header({ data }) {
    return (
        <header>
            <img src="/undead-people-logo.png" />
            <div className="header-text">
                <h1>Undead People</h1>
                <p>Developers</p>
            </div>
        </header>
    )
}

export default Header
