import './EmojiButton.css'

function EmojiButton({ data }) {
    return (
        <div class="btn emoji-button" data-clipboard-text={data.text}>
            <div className="emoji-container">
                <p className="emoji">{data.emoji}</p>
            </div>
            <div className="emoji-info">
                <p className="description">{data.description}</p>
                <p className="text">{data.text}</p>
            </div>
        </div>
    )
}

export default EmojiButton
