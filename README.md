# Guideline
This guideline serves as a reference for all projects developed and tools used by the Undead People Dev team. We value consistency, so we require that our developers try their best to keep up with our guidelines. Administrators and project managers have the responsability to maintain all projects consistent.

Check this project's wiki to access all of our guidelines.
